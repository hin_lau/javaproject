package com.hin;

public class Service implements Interface {
	
	public int add(int x, int y) {
		return x + y;
	}
	
	public int subtract(int x, int y) {
		return x - y;
	}
	
	public int output() {
		return 1;
	}
	
	public String version() {
		return "1.1";
	}
}